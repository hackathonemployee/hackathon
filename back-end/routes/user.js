const express = require('express');
const router = express.Router();
const bcrypt = require('bcryptjs');
const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');
const sendMail = require('../service/sendMail');

// Load Model
const User = require('../models/user');
const Admin = require('../models/admin');

router.post('/signup', (req, res, next) => {
    User.find({email:req.body.adminEmail})
    .exec()
    .then( user => {
        if(user.length >=1){
            res.status(200).json({
                message:'User Already Registered!',
                user:user
            });
        }
        else{
            bcrypt.hash(req.body.password, 10, (err, hash) => {
                if(err){
                    res.status(500).json({
                        message:'Password Error',
                        error:err
                    });
                }
                else{
                    const userDetail = new User({
                        email: req.body.adminEmail,
                        password:hash,
                        token: new mongoose.Types.ObjectId,
                        role:'admin'
                    });
                    userDetail
                    .save()
                    .then(result => {
                        const adminSchema = new Admin({
                            name: req.body.adminName,
                            email: req.body.adminEmail,
                            contact_no: req.body.contactNo
                        })
                        adminSchema
                        .save()
                        .then(result => {
                            sendMail(result.email, 'Signup Successful', 'You have successfully signed up for Employee Management System')
                            res.status(200).json({
                                message:'User Created',
                                user:result
                            });
                        })
                        .catch(error => {
                            res.status(500).json({
                                message:'Error Ocurred',
                                error:error
                            });
                        })
                    })
                    .catch(err => {
                        res.status(500).json({
                            message:'Error Ocurred',
                            error:err
                        });
                    });
                }
            })
        }
    })
});

router.post('/login', (req, res, next) => {
    User.findOne({email: req.body.userEmail})
    .exec()
    .then(user => {
        if(!user){
            return res.status(404).json({
                message:'User Not Found'
            });
        }
        else if(!user.password){
            return res.status(400).json({
                message:'Reset your password to set new password'
            });
        }
        else{
            bcrypt.compare(req.body.password, user.password, (err,result) =>{
                if(err){
                    return res.status(500).json({
                        message:'Error Ocurred',
                        error:err
                    });
                }
                if(result){
                    const token = jwt.sign({
                        email:user.email,
                        userId:user._id,
                        role:user.role
                    },
                    process.env.JWT_KEY,
                    {
                        expiresIn: '10h'
                    });
                    return res.status(200).json({
                        message:'Auth Successful',
                        token:token
                    });
                }
                else{
                    return res.status(400).json({
                        message:'Bad Auth'
                    });
                }
            });
        }
    })
    .catch(err => {
        return res.status(500).json({
            message:'Error Ocurred',
            error:err
        });
    })
})

router.post('/forget-password', (req, res, next)  => {
    User.findOne({'email':req.body.userEmail})
    .exec()
    .then(user => {
        if(user){
            let path = '/user/reset-password/'+user.token
            sendMail(user.email, 'Reset Password', 'Request received for Forget Password, Click on the link given to reset your password.  '+path)
            return res.status(200).json({
                link:path
            });
        }
        else{
            return res.status(404).json({
                message:'User Not Found'
            });
        }
    })
    .catch(err => {
        return res.status(500).json({
            message:'Error Ocurred',
            error:err
        });
    })
});

router.post('/reset-password/:token', (req, res, next) => {
    bcrypt.hash(req.body.password, 10, (err, hash) => {
        if(err){
            return res.status(500).json({
                message:'Error Ocurred',
                error:err
            });
        }
        User.findOneAndUpdate(
            { token : req.params.token },
            {
                token: new mongoose.Types.ObjectId(),
                password: hash
            })
            .exec()
            .then(result => {
                if(result){
                    sendMail(result.email, 'Password Reset Successful', 'You have successfully Reset Your Password. Login to continue using service')
                    return res.status(200).json({
                        message:'Password Reset Successful'
                    });
                }
                else{
                    return res.status(404).json({
                        message:'Token Invalid'
                    });
                }
            })
            .catch(error => {
                res.status(500).json({
                    message:'Error Ocurred',
                    error:error
                })
            })
    });
    
});

module.exports = router;