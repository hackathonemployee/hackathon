const express = require('express');
const router = express.Router();
const sendMail = require('../service/sendMail');

// Middlewares
const verify = require('../middleware/check-auth');
const { checkAdmin, checkEmployee } = require('../middleware/check-role');

// Load Models
const Leave = require('../models/leave');
const Employee = require('../models/employee');

router.get('/', verify, (req, res, next) => {
    return res.status(200).json({
        "trial": "true",
        userData:req.userData
    }).end()
})

router.post('/request-leave', (req, res, next) => {
    const leave = new Leave({
        name : req.body.userName,
        reason : req.body.leaveReason,
        contact_no : req.body.userContactNo,
        employeeID : req.body.ID,
        date : req.body.leaveDate,
        required_days : req.body.leaveDays
    });
    try{
        leave.save((err, obj) => {
            if (err) {
              throw err;
            }
            else{
                return res.status(200).json({
                    message:'Leave Added'
                });
            }
        });
    }
    catch (err){
        return res.status(500).json({
            message:'Error Ocurred',
            error:err
        });
    }
});

router.get('/employeeDetails', verify, (req, res, next) => {
    Employee.findOne({ email:req.userData.email })
    .exec()
    .then(employee => {
      if(employee){
        return res.status(200).json({
          message:'Employee Found',
          employee:employee
        })
      }
      else{
        return res.status(404).json({
          message:'Employee Not Found'
        })
      }
    })
    .catch(err => {
      return res.status(500).json({
        message:'Error Ocurred',
        error:err
      })
    })
  });

module.exports = router;