const mongoose = require('mongoose');
const express = require('express');
const router = express.Router();
const sendMail = require('../service/sendMail');

// Middleware
const verify = require('../middleware/check-auth');
const { checkAdmin, checkEmployee } = require('../middleware/check-role');

// Load Models
const Leave = require('../models/leave');
const Employee = require('../models/employee');
const User = require('../models/user');
const Admin = require('../models/admin');

router.get('/', (req, res, next) => {
  return res.status(200).json({
        "trial": "true"
    }).end()
})

router.get('/request-leave-pending', (req, res, next) => {
    Leave.find({'status':false})
    .exec((err, result) => {
        if(err){
            return res.status(500).json({
                message:'Error Ocurred',
                result:result,
                error:err
            });
        }
        else{
            return res.status(200).json({
                message:'list here',
                result:result
            });
        }
    });
});

router.get('/request-approve-leave/:ID', (req, res, next) => {
    Leave.findOneAndUpdate({
            '_id':req.params.ID
        },
        {
            'status':true
        },
        (err, result) => {
            if(err){
                return res.status(500).json({
                    message:'Error Ocurred',
                    error:err
                });
            }
            else{
                return res.status(200).json({
                    message:'Approved Leave'
                })
            }
        })
});

router.post('/addEmployee', (req, res, next) => {
  Employee.find({ email: req.body.employeeEmail })
  .exec()
  .then(employees => {
    if (employees.length >= 1) {
        return res.status(200).json({
        message:'Employee Already Exist'
      })
    } else {
          const emp = new Employee({
            name: req.body.employeeName,
            email: req.body.employeeEmail,
            address: req.body.employeeAddress,
            phone: req.body.contactNo,
            salary: req.body.employeeSalary,
            team: req.body.employeeTeam
          });
          emp
            .save()
            .then(result => {
                const user = new User({
                  email: result.email,
                  token: new mongoose.Types.ObjectId,
                  role: 'employee'
                });
                user.save()
                .then(userResult => {
                  let link = '/user/reset-password/'+userResult.token
                  sendMail(userResult.email, 'Welcome to Employee Management System', 'You have been added to Employee Management System. Please click on the link given below to set your new password  '+link)
                  return res.status(200).json({
                    message: 'Employee Added',
                    result: result
                  });
                })
                .catch(error => {
                  return res.status(500).json({
                    message:'Error Ocurred',
                    error:error
                  });
                })
            })
            .catch(err => {
              return res.status(500).json({
                message:'Error Ocurred',
                error:err
              });
            })
      }
  });
});

router.get('/employeeList', (req, res, next) => {
  Employee.find({}, '_id name phone email')
  .exec()
  .then(employee => {
    if(employee){
      return res.status(200).json({
        message:'Employee List',
        employees:employee
      });
    }
    else{
      return res.status(404).json({
        message:'Employees Not Found'
      });
    }
  })
  .catch(err => {
    return res.status(500).json({
      message:'Error Ocurred',
      error:err
    });
  });
});

router.get('/findEmployee/:id', (req, res, next) => {
  Employee.find({ _id:req.params.id })
  .exec()
  .then(employee => {
    if(employee){
      return res.status(200).json({
        message:'Employee Found',
        employee:employee
      });
    }
    else{
      return res.status(404).json({
        message:'Employee Not Found'
      });
    }
  })
  .catch(err => {
    return res.status(404).json({
      message:'Error Ocurred',
      error:err
    });
  })
})

router.post('/updateEmployee/:id', (req, res, next) => {
  Employee.findOneAndUpdate({ _id: req.params.id },
    {
            name: req.body.employeeName,
            email: req.body.employeeEmail,
            address: req.body.employeeAddress,
            phone: req.body.contactNo,
            salary: req.body.employeeSalary,
            team: req.body.employeeTeam
    })
    .exec()
    .then(employee => {
      if(employee){
        sendMail(employee.email, 'Information Updated', 'Your Information has been updated successfully.')
        return res.status(200).json({
          message:'Employee Updated'
        });
      }
      else{
        return res.status(404).json({
          message:'Employee Not Found'
        });
      }
    })
    .catch(err => {
      return res.status(404).json({
        message:'Error Ocurred',
        error:err
      });
    })
})

router.get('/adminDetails', verify, (req, res, next) => {
  Admin.findOne({ email:req.userData.email })
  .exec()
  .then(admin => {
    if(admin){
      return res.status(200).json({
        message:'Admin Found',
        admin:admin
      })
    }
    else{
      return res.status(404).json({
        message:'Admin Not Found'
      })
    }
  })
  .catch(err => {
    return res.status(500).json({
      message:'Error Ocurred',
      error:err
    })
  })
});

module.exports = router;