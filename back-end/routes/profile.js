const multer = require('multer');
const express = require('express');
const router = express.Router();
const fs = require('fs');
const verify = require('../middleware/check-auth');

const storage = multer.diskStorage({
    destination: function(req, file, cb) {
      cb(null, './public/');
    },
    filename: function(req, file, cb) {
      cb(null, req.userData.userId+'.jpeg');
    }
});
const fileFilter = (req, file, cb) => {
    // reject a file
    if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
      cb(null, true);
    } else {
      cb(null, false);
    }
};
const upload = multer({
    storage: storage,
    limits: {
      fileSize: 1024 * 1024 * 5
    },
    fileFilter: fileFilter
});

router.post('/upload', verify, upload.single('profilePic'), (req, res, next) => {
    return res.send(req.file);
});

router.delete('/delete/:id', (req, res, next) => {
    var path = 'public/'+req.params.id+'.jpeg';
        try {
            fs.unlinkSync(path)
            return res.status(200).json({
                message:'Profile Photo Deleted'
            });
        } 
        catch(err) {
            return res.status(200).json({
                message:'Error Ocurred',
                error:err
            });
        }
});

module.exports = router;