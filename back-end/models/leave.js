const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const leaveSchema = new Schema(
    {
        name: {
            type: String,
            required: true
        },
        reason: {
            type: String,
            required: true
        },
        employeeID:{
            type: mongoose.Schema.Types.ObjectId,
            required: true
        },
        contact_no: {
            type: Number,
            required: true
        },
        date: {
            type: String,
            required: true
        },
        required_days: {
            type: Number,
            required: true
        },
        status: {
            type: Boolean,
            default: false,
            required: true
        }
    },
    {
        timestamps : true
    }
);

const Leave = mongoose.model('Leave', leaveSchema);
module.exports = Leave;
