const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userSchema = new Schema(
    {
        email:{
            type: String,
            required: true
        },
        password:{
            type: String
        },
        token: mongoose.Schema.Types.ObjectId,
        role:{
            type: String,
            required:true
        }
    },
    {
        timestamps:true
    }
);

const User = mongoose.model('User', userSchema);
module.exports = User;