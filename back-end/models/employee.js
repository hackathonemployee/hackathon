const mongoose = require('mongoose');

const empSchema = mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true,
        unique: true
    },
    address: [
          {
            Line1:   String,
            Line2:   String,
            city:    String,
            state:   String,
            country: String,
            pincode: Number
          }
        ],
    phone: {
        type: Number,
        required:true
    },
    salary: {
        type: Number,
        required:true
    },
    team: {
        type:String,
        required:true
    }
    },
    {
        timestamps:true
    }
);

const emp = mongoose.model('Employee', empSchema);
module.exports = emp;