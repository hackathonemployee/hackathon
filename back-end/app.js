// Libraries
const express = require('express');
const mongoose = require('mongoose');
const http = require('http');
const bodyParser = require('body-parser');
require('dotenv').config();
const {checkAdmin, checkEmployee} = require('./middleware/check-role');
const verify = require('./middleware/check-auth');

//Initialization
const app = express();
const server = http.createServer(app);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// Connect to MongoDB
mongoose
  .connect(
    process.env.MONGO_URI,
    { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true})
  .then(() => console.log('MongoDB Connected'))
  .catch(err => console.log(err));
mongoose.Promise = global.Promise;
mongoose.set('useFindAndModify', false);

//CORS Handeled
app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header(
      "Access-Control-Allow-Headers",
      "Origin, X-Requested-With, Content-Type, Accept, Authorization"
    );
    if (req.method === "OPTIONS") {
      res.header("Access-Control-Allow-Methods", "PUT, POST, PATCH, DELETE, GET");
      return res.status(200).json({});
    }
    next();
});

app.use('/static', express.static('public'));

// Routes
app.use('/admin', require('./routes/admin'));
app.use('/employee', require('./routes/employee'));
app.use('/user', require('./routes/user'));
app.use('/profile', require('./routes/profile'));

//404
app.use((req, res, next) => {
    res.status(404).send('Not found');
});


// Server Starting
const PORT = process.env.PORT || 5000;
server.listen(PORT, console.log(`Server started on port ${PORT}`));