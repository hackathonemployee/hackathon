const nodemailer = require('nodemailer');

module.exports = function (to, sub, body) {
    var transporter = nodemailer.createTransport({
        service: "gmail",
        auth: {
          user: process.env.EMAIL,
          pass: process.env.EMAIL_PASS 
        }
      });
      
      var mailOptions = {
        from: '"Employee Management System" <'+process.env.EMAIL+'>',
        to: to,
        subject: sub,
        html: body
      };
      
      transporter.sendMail(mailOptions, function(error, info){
        if (error) {
          return 0;
        } else {
          return 1;
        }
      });
}