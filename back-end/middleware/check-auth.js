const jwt = require('jsonwebtoken');

module.exports = (req, res, next) => {
    if(req.headers.authorization){
        const token = req.headers.authorization.split(' ')[1];
        try{
            const decoded = jwt.verify(token, process.env.JWT_KEY);
            req.userData = decoded;
            next();
        }
        catch(err){
            return res.status(500).json({
                message:'Error Ocurred'
            });
        }
    }
    else{
        return res.status(404).json({
            message:'Token Not Found'
        });
    }
}