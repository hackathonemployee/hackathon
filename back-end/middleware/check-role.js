function checkAdmin (req, res, next) {
    if(req.userData.role == 'admin'){
        next();
    }
    else{
        return res.status(403).json({
            message:'Access Forbidden'
        })
    }
}
function checkEmployee (req, res, next) {
    if(req.userData.role == 'employee'){
        next();
    }
    else{
        return res.status(403).json({
            message:'Access Forbidden'
        })
    }
}

module.exports = {checkAdmin, checkEmployee}